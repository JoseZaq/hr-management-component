package cat.itb.DAOFactory;
import cat.itb.depDAO.DepartamentoDAO;
import cat.itb.empDAO.EmpleadoDAO;

public abstract class DAOFactory {
    // Bases de datos soportadas
    public static final int POSTGRESQL = 1;
    public static final int NEODATIS = 2;
    public abstract DepartamentoDAO getDepartamentoDAO();
    public abstract EmpleadoDAO getEmpleadoDAO();
    public static DAOFactory getDAOFactory(int bd) {
        switch (bd) {
            case POSTGRESQL:
                return new SqlDbDAOFactory();
            case NEODATIS:
                return null; //new NeodatisDAOFactory();
            default :
                return null;
        }
    }
}