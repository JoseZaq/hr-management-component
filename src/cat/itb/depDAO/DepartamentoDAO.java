package cat.itb.depDAO;
//Interficie DAO
public interface DepartamentoDAO {
public boolean insertarDep(Departamento dep);
public boolean eliminarDep(int deptno);
public boolean modificarDep(Departamento dep);
public Departamento consultarDep(int deptno);
}