package cat.itb.empDAO;

import java.io.Serializable;
import java.sql.Date;

public class Empleado implements Serializable {
    int empno;
    String apellido;
    String oficio;
    int dir;
    Date fechaalt;
    double salario;
    double comision;
    int deptno;
    // constructor

    public Empleado() {
    }

    public Empleado(int empno, String apellido, String oficio, int dir, Date fechaalt, double salario, double comision, int deptno) {
        this.empno = empno;
        this.apellido = apellido;
        this.oficio = oficio;
        this.dir = dir;
        this.fechaalt = fechaalt;
        this.salario = salario;
        this.comision = comision;
        this.deptno = deptno;
    }

    // getters and setters


    public int getEmpno() {
        return empno;
    }

    public void setEmpno(int empno) {
        this.empno = empno;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getOficio() {
        return oficio;
    }

    public void setOficio(String oficio) {
        this.oficio = oficio;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public Date getFechaalt() {
        return fechaalt;
    }

    public void setFechaalt(Date fechaalt) {
        this.fechaalt = fechaalt;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public double getComision() {
        return comision;
    }

    public void setComision(double comision) {
        this.comision = comision;
    }

    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }
}
