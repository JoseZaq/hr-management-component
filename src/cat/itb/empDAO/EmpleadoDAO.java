package cat.itb.empDAO;


public interface EmpleadoDAO {
    public boolean insertarEmp(Empleado emp);
    public boolean eliminarEmp(int empno);
    public boolean modificarEmp(Empleado emp);
    public Empleado consultarEmp(int empno);
}
