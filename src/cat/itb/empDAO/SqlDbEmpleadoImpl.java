package cat.itb.empDAO;

import cat.itb.DAOFactory.SqlDbDAOFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlDbEmpleadoImpl implements EmpleadoDAO{
    Connection conexion;
    public SqlDbEmpleadoImpl() {
        conexion = SqlDbDAOFactory.crearConexion();
    }
    public boolean insertarEmp(Empleado emp) {
        boolean valor = false;
        String sql = "INSERT INTO empleados VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, emp.getEmpno());
            sentencia.setString(2, emp.getApellido());
            sentencia.setString(3, emp.getOficio());
            sentencia.setInt(4, emp.getDir());
            sentencia.setDate(5, emp.getFechaalt());
            sentencia.setDouble(6, emp.getSalario());
            sentencia.setDouble(7, emp.getComision());
            sentencia.setInt(8, emp.getDeptno());
            int filas = sentencia.executeUpdate();
            //System.out.printf("Filas insertadas: %d%n", filas);
            if (filas > 0) {
                valor = true;
                System.out.printf("Empleado %d insertado%n", emp.getEmpno());
            }
            sentencia.close();
        } catch (SQLException e) {
            mensajeExcepcion(e);
        }
        return valor;
    }
    @Override
    public boolean eliminarEmp(int empno) {
        boolean valor = false;
        String sql = "DELETE FROM empleados WHERE emp_no = ? ";
        PreparedStatement sentencia;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, empno);
            int filas = sentencia.executeUpdate();
            //System.out.printf("Filas eliminadas: %d%n", filas);
            if (filas > 0) {
                valor = true;
                System.out.printf("Empleado %d eliminado%n", empno);
            }
            sentencia.close();
        } catch (SQLException e) {
            mensajeExcepcion(e);
        }
        return valor;
    }
    @Override
    public boolean modificarEmp(Empleado emp) {
        boolean valor = false;
        String sql = "UPDATE empleados SET apellido= ?, oficio = ?, dir= ?, " +
                "fecha_alt = ?, salario = ?, comision = ?," +
                "dept_no = ? WHERE emp_no = ? ";
        PreparedStatement sentencia;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(8, emp.getEmpno());
            sentencia.setString(1, emp.getApellido());
            sentencia.setString(2, emp.getOficio());
            sentencia.setInt(3, emp.getDir());
            sentencia.setDate(4, emp.getFechaalt());
            sentencia.setDouble(5, emp.getSalario());
            sentencia.setDouble(6, emp.getComision());
            sentencia.setInt(7, emp.getDeptno());
            int filas = sentencia.executeUpdate();
            //System.out.printf("Filas modificadas: %d%n", filas);
            if (filas > 0) {
                valor = true;
                System.out.printf("Empleado %d modificado%n", emp.getEmpno());
            }
            sentencia.close();
        } catch (SQLException e) {
            mensajeExcepcion(e);
        }
        return valor;
    }
    @Override
    public Empleado consultarEmp(int empno) {
        String sql = "SELECT * FROM empleados WHERE emp_no = ?";
        PreparedStatement sentencia;
        Empleado emp = new Empleado();
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, empno);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                emp.setEmpno(rs.getInt("emp_no"));
                emp.setApellido(rs.getString("apellido"));
                emp.setOficio(rs.getString("oficio"));
                emp.setDir(rs.getInt("dir"));
                emp.setFechaalt(rs.getDate("fecha_alt"));
                emp.setSalario(rs.getDouble("salario"));
                emp.setComision(rs.getDouble("comision"));
                emp.setDeptno(rs.getInt("dept_no"));
            }
            else
                System.out.printf("Empleado: %d No existe%n",empno);
            rs.close();// liberar recursos
            sentencia.close();
        } catch (SQLException e) {
            mensajeExcepcion(e);
        }
        return emp;
    }
    private void mensajeExcepcion(SQLException e) {
        System.out.printf("HA OCURRIDO UNA EXCEPCIÓN:%n");
        System.out.printf("Mensaje : %s %n", e.getMessage());
        System.out.printf("SQL estado: %s %n", e.getSQLState());
        System.out.printf("Cód error : %s %n", e.getErrorCode());
    }
}
